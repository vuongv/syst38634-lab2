
public class main {

	public static void main(String[] args) {
		int[] numArray = new int[3];
		numArray[0] = 25;
		numArray[1] = 45;
		numArray[2] = 65;
		
		
		String evenString = "Khoa";
		String oddString = "350";
		
		String test = "w3rsource.com";
		System.out.println(UnicodeCodePointCounting(test));
				
	}
	public static int lowestNum(int[] numArray) {
		int lowestNum = numArray[0];
		for(int i = 1; i<numArray.length;i++) {
			if (lowestNum > numArray[i]) {
				lowestNum = numArray[i];
			}
		}
		return lowestNum;
	}
	
	public static int averageNum(int[] numArray) {
		int sum = 0;
	
		for (int i = 0; i<numArray.length;i++) {
			sum = sum+ numArray[i];
		}
		return sum/numArray.length;
	}
	public static String middleCharacter(String sequence) {
		String returnMiddle = "";
		double position =sequence.length()/2;
		
		if (sequence.length()%2 != 0) {
			System.out.println("odd");
			char middle = sequence.charAt((int) position);
			returnMiddle += middle;
		} else if (sequence.length()%2 ==0 ){
			System.out.println("even");
			int middle1 = (int) Math.ceil(position);
			int middle2 = (int) Math.floor(position);
			char middle1Char = sequence.charAt(middle1-1);
			char middle2Char = sequence.charAt(middle2);
			returnMiddle +=middle1Char;
			returnMiddle +=middle2Char;
		}
		return returnMiddle;
	}
	public static int vowelCount(String sequence) {
		int counter = 0;
		for(int i = 0; i< sequence.length();i++) {
			String current = sequence.substring(i,i+1);
			if ("a".equals(current) || "A".equals(current)) {
				counter++;
			}else if ("e".equals(current)) {
				counter++;
			}else if ("i".equals(current) || "I".equals(current)) {
				counter++;
			}else if ("o".equals(current) || "O".equals(current)) {
				counter++;
			}else if ("u".equals(current) || "U".equals(current)) {
				counter++;
			}
		}
		return counter;
	}
	public static char indexAt(String sequence, int index) {
		return sequence.charAt(index);
		}
	public static int UnicodeCodePointCounting(String sequence) {
		return sequence.codePointCount(0, sequence.length());
	}
}
